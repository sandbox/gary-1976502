<?php //print $messages; ?>
<?php //print render($title_prefix); ?>
<?php if ($title && false): ?><h1 id="page-title"><?php print $title; ?></h1><?php endif; ?>
<?php //print render($title_suffix); ?>
<?php //echo  theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb())); ?>
<?php if ($tabs = render($tabs) && false): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
<?php if ($action_links = render($action_links) && false): ?><ul class="action-links"><?php print $action_links; ?></ul><?php endif; ?>
<?php print render($page['content']); ?>
