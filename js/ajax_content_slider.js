(function($) {
  $.fn.slideLoader = function(options) {
    // target object
    var that = $(this);
    $('body').addClass('ajax-content-slider-processed');
    //some default css rules
    that.css('left', "0%");
    // set position depend on actual browser size
    $(window).resize(function() {
      that.scrollLeft($('.active-ajax-slider').attr('rel') * $(window).width());
      that.scrollTop(0);
      if (options.resize) {
        options.resize();
      }
    });
    if (options.init) {
      options.init();
    }
    // every anchor which has defined class
    $('a.' + options.class).once('once').click(function(e) {
        e.preventDefault();
        // generate individual id
        var href = $(this).attr('href');
        var id = href;
        var Re = new RegExp("\\/","g");
        id = id.replace(Re, '-');
        
        // visited url, already loaded page
        if ($('#slide' + id).length) {          
          var state = {};
          state['slideloader'] = id;
          history.pushState(state, "", href);
          
          $('.active-ajax-slider').removeClass('active-ajax-slider');
          $('#slide' + id).addClass('active-ajax-slider');
          
          // scroll to existing DOM object
          that.stop().animate({
            scrollLeft : $('#slide' + id).attr('rel') * $(window).width(),
            scrollTop : 0,
          }, 1000);
        }
        // new url
        else {
          // load content
          var suffix = '';
          if (href.indexOf('?') > -1) {
            suffix = '&ajax=1';
          }
          else {
            suffix = '?ajax=1';
          }
          $.ajax({
            url: href + suffix,
            type : 'POST',
            success : function(html) {
              
              var pages = 1;
              
              $('.ajax-content-slider').each(function() {
                if (parseInt($(this).attr('rel')) > pages) {
                  pages = parseInt($(this).attr('rel'));
                }
              });
              pages++;
              // create container DOM object
              that.append('<div id="slide' + id + '" class="ajax-content-slider"></div>');
              $('#slide' + id).css('left', pages + "00%");
              $('#slide' + id).html(html);
              $('#slide' + id).attr('rel', pages);
              
              var state = {};
              state['slideloader'] = id;
              history.pushState(state, "", href);
              // move to new div
              $('.active-ajax-slider').removeClass('active-ajax-slider');
              $('#slide' + id).addClass('active-ajax-slider');
              that.stop().animate({
                scrollLeft : $('#slide' + id).attr('rel') * $(window).width(),
                scrollTop: 0,
              }, 1000);

              Drupal.attachBehaviors($('#slide' + id));
              // if callback provided call it.
              if (options.callback) {
                options.callback();
              }
            },
          });
        }
    });
    
    //handle history change
    window.onpopstate = function(e) {
      if (e.state && e.state.slideloader) {
        $('.active-ajax-slider').removeClass('active-ajax-slider');
        $('#slide' + e.state.slideloader).addClass('active-ajax-slider');
        that.stop().animate({
          scrollLeft : $('#slide' + e.state.slideloader).attr('rel') * $(window).width(),
          scrollTop: 0,
        }, 1000);
      }
      else {
        $('.active-ajax-slider').removeClass('active-ajax-slider');
        that.stop().animate({
          scrollTop: 0,
          scrollLeft: 0,
        }, 1000);
      }
    };
  };
})(jQuery);